define([], function(){
  var statuses = {};

  statuses.STATUS_ONLINE = 1,
  statuses.STATUS_OFFLINE = 2,
  statuses.STATUS_BUSY = 3,
  statuses.STATUS_AWAY = 4;

  statuses.getIcoName = function(status) {
    switch (status) {
      case this.STATUS_ONLINE:
        return 'online'
        break;
      case this.STATUS_OFFLINE:
        return 'offline'
        break;
      case this.STATUS_BUSY:
        return 'disturb'
        break;
      case this.STATUS_AWAY:
        return 'away'
        break;
    }

    return undefined;
  }

  statuses.getStatusDescription = function(status) {
    switch (status) {
      case this.STATUS_ONLINE:
        return 'В сети'
        break;
      case this.STATUS_OFFLINE:
        return 'Не в сети'
        break;
      case this.STATUS_BUSY:
        return 'Занят'
        break;
      case this.STATUS_AWAY:
        return 'Отошел'
        break;
    }

    return undefined;
  }

  return statuses;
});

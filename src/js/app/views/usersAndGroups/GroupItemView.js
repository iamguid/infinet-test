define([
  'jquery',
  'underscore',
  'backbone',
  'statuses',
  'text!templates/usersAndGroups/GroupItemTemplate.html',
], function($, _, Backbone, statuses, GroupItemTemplate){

  var GroupItemView = Backbone.View.extend({
    events: {
      'click .users-overview__user-delete-btn' : 'delete',
      'click .users-overview__user-edit-btn' : 'edit'
    },

    // Delete model and view
    delete: function () {
      this.model.destroy();
      this.remove();
      return false;
    },

    edit: function (e) {
      window.App.router.navigate('#/user/edit/' + this.model.get('id'));
      e.stopPropagation()
      return false;
    },

    render: function () {
      var groupItemCompiledTemplate = _.template(GroupItemTemplate);
      var $groupItemElement = $(groupItemCompiledTemplate({
        user: {
          name: this.model.sliceName(),
          avatar_src: this.model.get('avatar_src'),
          nubmers: this.model.get('numbers'),
          post: this.model.get('post'),
          icoStatus: statuses.getIcoName(this.model.get('status_id')),
          status: statuses.getStatusDescription(this.model.get('status_id'))
        }
      }));

      this.setElement($groupItemElement);

      return this;
    }
  });

  return GroupItemView;
});

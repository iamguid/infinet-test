define([
  'jquery',
  'underscore',
  'backbone',
  'statuses',
  'collections/UserCollection',
  'collections/GroupCollection',
  'collections/FilteredCollection',
  'views/usersAndGroups/GroupView',
  'text!templates/usersAndGroups/OverviewTemplate.html',
], function($, _, Backbone, statuses, UserCollection, GroupCollection, FilteredCollection, GroupView, OverviewTemplate){

  var OverviewView = Backbone.View.extend({
    events: {
      'click .users-overview__filter-btns' : 'setStatusFilter',
      'keyup .users-overview__search > input' : 'setNameFilter',
    },

    initialize: function () {
      this.userCollection = new UserCollection();
      this.groupCollection = new GroupCollection();

      this.filteredUserCollection = FilteredCollection(this.userCollection);
      this.filteredGroupCollection = FilteredCollection(this.groupCollection);

      this.listenTo(this.userCollection, 'all', this.render);
      this.listenTo(this.groupCollection, 'all', this.render);
      this.listenTo(this.filteredUserCollection, 'change remove', this.render);
      this.listenTo(this.filteredGroupCollection, 'change remove', this.render);

      this.groupView = new GroupView({
        filteredUserCollection: this.filteredUserCollection,
        filteredGroupCollection: this.filteredGroupCollection,
      });

      this.userCollection.fetch();
      this.groupCollection.fetch();
    },

    setNameFilter: function (e) {
      var searchValue = e.currentTarget.value;
      this.filteredUserCollection.filterBy(function (user) {
        return user.sliceName().indexOf(searchValue) > -1;
      });

      this.resetBtns($('.users-overview__filter-btns'));
      this.renderGroups();
    },

    setStatusFilter: function (e) {
      var $target = $(e.target);

      if ($target.hasClass('filter-all')) {
        this.filteredUserCollection.filterBy();
      } else if ($target.hasClass('filter-online')) {
        this.filteredUserCollection.filterBy({
          status_id: statuses.STATUS_ONLINE
        });
      } else if ($target.hasClass('filter-away')) {
        this.filteredUserCollection.filterBy({
          status_id: statuses.STATUS_AWAY
        });
      } else if ($target.hasClass('filter-busy')) {
        this.filteredUserCollection.filterBy({
          status_id: statuses.STATUS_BUSY
        });
      } else if ($target.hasClass('filter-offline')) {
        this.filteredUserCollection.filterBy({
          status_id: statuses.STATUS_OFFLINE
        });
      }

      this.renderGroups();
    },

    resetBtns: function ($wrapper) {
      $wrapper.children().each(function (key, btn) {
        $(btn).removeClass('active');
      })
    },

    setCountLabel: function (el, val) {
      $el = $(el);
      $count = $el.find('span.count');

      if (!$count.length) {
        $count = $(document.createElement( "span" ));
        $count.addClass('count');
        $el.append($count);
      }

      $count.text('(' + val + ')')
    },

    updateFilterBtns: function () {
      var $filterButtons = $(this.$el.find(".users-overview__filter-btns"));

      var allFilterBtn = $filterButtons.find(".filter-all");
      var onlineFilterBtn = $filterButtons.find(".filter-online");
      var awayFilterBtn = $filterButtons.find(".filter-away");
      var busyFilterBtn = $filterButtons.find(".filter-busy");
      var offlineFilterBtn = $filterButtons.find(".filter-offline");

      this.setCountLabel(
        allFilterBtn,
        this.userCollection.length
      );

      this.setCountLabel(
        onlineFilterBtn,
        this.userCollection.where({
          status_id: statuses.STATUS_ONLINE
        }).length
      );

      this.setCountLabel(
        awayFilterBtn,
        this.userCollection.where({
          status_id: statuses.STATUS_AWAY
        }).length
      );

      this.setCountLabel(
        busyFilterBtn,
        this.userCollection.where({
          status_id: statuses.STATUS_BUSY
        }).length
      );

      this.setCountLabel(
        offlineFilterBtn,
        this.userCollection.where({
          status_id: statuses.STATUS_OFFLINE
        }).length
      );
    },

    render: function () {
      var overviewCompiled = _.template(OverviewTemplate);
      this.$el.html(overviewCompiled);
      this.$el.prepend('<h2 class="page-content__main-title">Пользователи и группы</h2>')

      this.renderGroups();
      this.updateFilterBtns();

      return this;
    },

    renderGroups: function () {
      var self = this;
      var $groupsWrapper = self.$('.users-overview__groups');

      // Clear wrapper
      $groupsWrapper.html('');

      // Each groups
      _.each(this.filteredGroupCollection.models, function (group, key) {
        var filterUsersByGroup = self.filteredUserCollection.where({
          group_id: group.get('id')
        });

        if (!filterUsersByGroup.length) return;

        var groupView = new GroupView({
          title: group.get('name'),
          models: filterUsersByGroup
        });

        $groupsWrapper.append(groupView.render().el);
      });
    },
  });

  return OverviewView;
});

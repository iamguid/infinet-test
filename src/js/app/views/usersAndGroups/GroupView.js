define([
  'jquery',
  'underscore',
  'backbone',
  'statuses',
  'views/usersAndGroups/GroupItemView',
  'text!templates/usersAndGroups/GroupTemplate.html',
], function($, _, Backbone, statuses, GroupItemView, GroupTemplate){

  var GroupView = Backbone.View.extend({

    events: { },

    initialize: function (options) {
      this.title = options.title;
      this.models = options.models;
    },

    render: function () {
      var groupCompiledTemplate = _.template(GroupTemplate);
      var $groupElement = $(groupCompiledTemplate({
        title: this.title
      }));

      var groupItemsWrapperSelector = '.users-overview__table > tbody';
      var $groupItemsWrapper = $groupElement.find(groupItemsWrapperSelector);

      // Each users
      _.each(this.models, function (model) {
        var groupItemView = new GroupItemView({
          model: model
        });

        $groupItemsWrapper.append(groupItemView.render().el);
      })

      this.$el.html($groupElement);

      return this;
    }
  });

  return GroupView;
});

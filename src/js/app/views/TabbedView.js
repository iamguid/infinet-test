define([
  'jquery',
  'underscore',
  'backbone',
  'text!templates/TabbedTemplate.html',
], function($, _, Backbone, TabbedTemplate){

  var TabbedView = Backbone.View.extend({
    events: { },

    initialize: function(tabs) {
      this.tabs = tabs;
    },

    render: function () {
      var tabbedCompiledTemplate = _.template(TabbedTemplate);
      var $tabbedElement = $(tabbedCompiledTemplate());

      var $nav = $tabbedElement.find('.tab-nav');
      var $body = $tabbedElement.find('.tab-body');

      var self = this;
      _.each(self.tabs, function (tab, key) {
        var id = 'tab-pane-' + key;
        $nav.append(self.createNavItem(tab.caption, id));
        $body.append(self.createTabPane(tab.view, id));
      });

      this.$el.html($tabbedElement);

      // Activate first
      $($nav.find('li:first'))
        .addClass('active')
        .find('a:first')
        .addClass('active');

      $($body.find('.tab-pane:first'))
        .addClass('active');

      return this;
    },

    createNavItem: function (caption, targetId) {
      var $navItem = $('<li></li>');
      var $navItemLink = $('<a></a>')
        .attr('data-toggle', 'tab')
        .attr('href', '#' + targetId)
        .text(caption);

      $navItem.append($navItemLink);

      return $navItem;
    },

    createTabPane: function (view, id) {
      var view = view;
      var $pane = $('<div></div>')
        .addClass('tab-pane')
        .attr('id', id)
        .append(view.render().el);

      return $pane;
    }
  });

  return TabbedView;
});

define([
  'jquery',
  'underscore',
  'backbone',
  'statuses',
  'text!templates/userEdit/TabCallRulesTemplate.html',
  'text!templates/userEdit/TabCallRulesItemTemplate.html'
], function($, _, Backbone, statuses, TabCallRulesTemplate, TabCallRulesItemTemplate){

  var TabCallRulesView = Backbone.View.extend({
    events: { },

    render: function () {
      var self = this;

      var tabCallRulesCompiledTemplate = _.template(TabCallRulesTemplate);
      var $tabCallRulesElement = $(tabCallRulesCompiledTemplate());

      this.setElement($tabCallRulesElement);

      // loop numbers
      _.each(this.model.get('numbers'), function (number) {
        var itemCompiledTemplate = _.template(TabCallRulesItemTemplate);
        var $itemElement = $(itemCompiledTemplate({
          number: number
        }));

        self.$el.find('.call-rules__table').append($itemElement);
      })

      return this;
    }
  });

  return TabCallRulesView;
});

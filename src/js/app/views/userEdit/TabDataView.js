define([
  'jquery',
  'underscore',
  'backbone',
  'collections/RoleCollection',
  'collections/GroupCollection',
  'text!templates/userEdit/TabDataTemplate.html'
], function($, _, Backbone, RoleCollection, GroupCollection, TabDataTemplate){

  var TabDataView = Backbone.View.extend({
    events: {
    },

    render: function () {
      var tabDataCompiledTemplate = _.template(TabDataTemplate);
      var $tabDataElement = $(tabDataCompiledTemplate());

      this.setElement($tabDataElement);

      this.renderSelects();
      this.fillInputs();
      return this;
    },

    renderSelects: function () {
      var self = this;

      var roles = new RoleCollection();
      roles.fetch({
        success: function () {
          var $select = $(self.$el.find('#selectRole'))

          _.each(roles.models, function (role) {
            var option = $('<option></option>')
              .attr('value', role.get('id'))
              .text(role.get('name'));
            $select.append(option);
          })
        }
      })

      var groups = new GroupCollection();
      groups.fetch({
        success: function () {
          var $select = $(self.$el.find('#selectGroup'))

          _.each(groups.models, function (group) {
            var option = $('<option></option>')
              .attr('value', group.get('id'))
              .text(group.get('name'));
            $select.append(option);
          })
        }
      })
    },

    fillInputs: function () {
      var $surName = $(this.$el.find('#inputSurname'))
        .val(this.model.get('surname'));

      var $firstName = $(this.$el.find('#inputFirstname'))
        .val(this.model.get('firstname'));

      var $lastName = $(this.$el.find('#inputLastname'))
        .val(this.model.get('lastname'));

      var $birthday = $(this.$el.find('#inputBirthday'))
        .val(this.model.get('birthday'));

      var $role = $(this.$el.find('#selectRole'))
        .val(this.model.get('role_id'));

      var $login = $(this.$el.find('#inputHiusLogin'))
        .val(this.model.get('hius_login'));

      var $group = $(this.$el.find('#selectGroup'))
        .val(this.model.get('group_id'));

      var $post = $(this.$el.find('#inputPost'))
        .val(this.model.get('post'));

      var $comment = $(this.$el.find('#textareaComment'))
        .val(this.model.get('comment'));

      var $avatar = $(this.$el.find('#imgAvatar'))
        .attr('src', this.model.get('avatar_src'));
    }
  });

  return TabDataView;
});

define([
  'jquery',
  'underscore',
  'backbone',
  'statuses',
  'views/TabbedView',
  'views/userEdit/TabDataView',
  'views/userEdit/TabCallRulesView',
  'text!templates/userEdit/EditTemplate.html'
], function($, _, Backbone, statuses, TabbedView, TabDataView, TabCallRulesView,  EditTemplate){

  var UserEditView = Backbone.View.extend({
    events: {
      'click .user-edit__cancel-btn' : 'cancel',
      'click .user-edit__save-btn' : 'save',
    },

    cancel: function (e) {
      window.App.router.navigate('#/users');
      e.stopPropagation()
      return false;
    },

    // TODO: Implement function
    save: function (e) {
      window.App.router.navigate('#/users');
      e.stopPropagation()
      return false;
    },

    render: function () {
      var tabs = [
        {
          caption: 'Регистрационные данные',
          view: new TabDataView({
            model: this.model
          })
        },
        {
          caption: 'Правила вызова',
          view: new TabCallRulesView({
            model: this.model
          })
        }
      ];

      var tabbedView = new TabbedView(tabs);

      var editCompiledTemplate = _.template(EditTemplate);
      var $editElement = $(editCompiledTemplate());

      this.$el.html($editElement);
      this.$el.find('.user-edit__tabs').append(tabbedView.render().el);

      return this;
    }
  });

  return UserEditView;
});

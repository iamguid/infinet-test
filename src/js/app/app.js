define([
  'jquery',
  'underscore',
  'backbone',
  'router',
  'bsButton',
  'bsTab',
], function($, _, Backbone, Router, bsButton, bsTab){

  var initialize = function(){

    // TODO: Ugly hack. Require app not worked
    window.App = {};
    window.App.router = Router.initialize();
  }

  return {
    initialize: initialize
  };
});

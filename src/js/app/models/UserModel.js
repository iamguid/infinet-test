define([
  'underscore',
  'backbone'
], function(_, Backbone){

  var UserModel = Backbone.Model.extend({
    sliceName: function () {
      var sur = this.get('surname');
      var first = this.get('firstname');
      var last = this.get('lastname');

      return sur + ' ' + first + ' ' + last;
    }
  });

  return UserModel;
});

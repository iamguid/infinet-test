define([
  'jquery',
  'underscore',
  'backbone',
  'collections/UserCollection',
  'models/UserModel',
  'views/usersAndGroups/OverviewView',
  'views/userEdit/EditView',
], function($, _, Backbone, UserCollection, UserModel, OverviewView, UserEditView){

  var AppRouter = Backbone.Router.extend({
    routes: {
      // Define some URL routes
      '': 'showUsersOverview',
      'users': 'showUsersOverview',
      'user/edit/:userId': 'showUserEdit',
      'user/create': 'showUserCreate',

      // Default
      // '*actions': 'showUsersOverview'
    },

    showUsersOverview: function () {
      var overviewView = new OverviewView();
      $('.page-content__main').html(overviewView.render().el);
    },

    showUserEdit: function (userId) {
      var collection = new UserCollection();
      collection.fetch({
        success: function () {
          var view = new UserEditView({
            model: collection.findWhere({
              id: +userId
            })
          });
          $('.page-content__main').html(view.render().el);
        }
      });
    },

    showUserCreate: function () {
      var model = new UserModel();
      var view = new UserEditView({
        model: model
      });

      $('.page-content__main').html(view.render().el);
    }
  });

  var initialize = function(){
    var appRouter = new AppRouter();
    Backbone.history.start();

    return appRouter;
  };

  return {
    initialize: initialize
  };
});

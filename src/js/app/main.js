require.config({
  shim : {
    "bsButton" : { "deps" :['jquery'] },
    "bsTab" : { "deps" :['jquery'] },
  },
  paths: {
    jquery: '../vendor/jquery',
    underscore: '../vendor/underscore',
    backbone: '../vendor/backbone',
    text: '../vendor/underscore.text',
    bsButton: '../vendor/bootstrap.button',
    bsTab: '../vendor/bootstrap.tab',
  }
});

require([
  // Load our app module and pass it to our definition function
  'app',
], function(App){

  // The "app" dependency is passed in as "App"
  App.initialize();
});

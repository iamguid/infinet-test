define([
  'underscore',
  'backbone',
], function(_, Backbone){

  function FilteredCollection (original) {
    var filtered = new original.constructor();

    // allow this object to have it's own events
    filtered._callbacks = {};

    // call 'filterBy' on the original function so that
    // filtering will happen from the complete collection
    filtered.filterBy = function(criteria){
      var items;

      // call 'where' if we have criteria
      // or just do nothing
      if (criteria){
        if (_.isFunction(criteria)){
          items = original.filter(criteria);
        } else if (_.isObject(criteria)) {
          items = original.where(criteria);
        }
      } else {
        items = original.models;
      }

      // store current criteria
      filtered._currentCriteria = criteria;

      // reset the filtered collection with the new items
      filtered.reset(items);
    };

    // when the original collection is reset,
    // the filtered collection will re-filter itself
    // and end up with the new filtered result set
    original.on("change sync", function(){
      filtered.filterBy(filtered._currentCriteria);
    });

    return filtered;
  }

  return FilteredCollection;
});

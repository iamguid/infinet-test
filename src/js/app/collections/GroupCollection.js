define([
  'underscore',
  'backbone',
  'models/GroupModel',
], function(_, Backbone, GroupModel){

  var GroupCollection = Backbone.Collection.extend({
    model: GroupModel,
    url: '/server/groups.json'
  });

  return GroupCollection;
});

define([
  'underscore',
  'backbone',
  'models/RoleModel',
], function(_, Backbone, RoleModel){

  var RoleCollection = Backbone.Collection.extend({
    model: RoleModel,
    url: '/server/user_roles.json',
  });

  return RoleCollection;
});

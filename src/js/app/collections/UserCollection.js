define([
  'underscore',
  'backbone',
  'models/UserModel',
  'statuses'
], function(_, Backbone, UserModel, statuses){

  var UserCollection = Backbone.Collection.extend({
    model: UserModel,
    url: '/server/users.json',
  });

  return UserCollection;
});

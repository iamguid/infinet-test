var fs = require('fs');
var path = require('path');

var gulp = require('gulp');
var merge = require('merge-stream');
var saveLicense = require('uglify-save-license');

// Load all gulp plugins automatically
// and attach them to the `plugins` object
var plugins = require('gulp-load-plugins')();

// Temporary solution until gulp 4
// https://github.com/gulpjs/gulp/issues/355
var runSequence = require('run-sequence');

var node_config = require('./package.json');
var bower_config = require('./bower.json');
var project = require('./project.json');

var dirs = project['configs'].directories;

// ---------------------------------------------------------------------
// | Helper tasks                                                      |
// ---------------------------------------------------------------------

gulp.task('archive:create_archive_dir', function () {
    fs.mkdirSync(path.resolve(dirs.archive), '0755');
});

gulp.task('archive:zip', function (done) {
  var archiveName = path.resolve(dirs.archive, project.name + '_v' + project.version + '.zip');
  var archiver = require('archiver')('zip');
  var files = require('glob').sync('**/*.*', {
    'cwd': dirs.dist,
    'dot': true // include hidden files
  });
  var output = fs.createWriteStream(archiveName);

  archiver.on('error', function (error) {
    done();
    throw error;
  });

  output.on('close', done);

  files.forEach(function (file) {
    var filePath = path.resolve(dirs.dist, file);

    // `archiver.bulk` does not maintain the file
    // permissions, so we need to add files individually
    archiver.append(fs.createReadStream(filePath), {
      'name': file,
      'mode': fs.statSync(filePath)
    });

  });

  archiver.pipe(output);
  archiver.finalize();
});

gulp.task('clean', function (done) {
  require('del')([
    dirs.dist
  ], done);
});

// Replace global constants by string
gulp.task('setGlobs', function () {
  var jquery_version = require('./bower_components/jquery/bower.json').version;

  return gulp.src([
    // Replace files path
    dirs.dist + '/css/main.css',
    dirs.dist + '/*.html'
  ], { base: dirs.dist + '/' })
    .pipe(plugins.replace(/{{IMG_PATH}}/g, '/img'))
    .pipe(plugins.replace(/{{IMAGES_PATH}}/g, '/images'))
    .pipe(plugins.replace(/{{FONTS_PATH}}/g, '../fonts'))
    .pipe(plugins.replace(/{{JQUERY_VERSION}}/g, jquery_version))
    .pipe(gulp.dest(dirs.dist));
});


// See my fork https://github.com/iamguid/css3FontConverter
// Run this task after copy all fonts to dist folder"
gulp.task('genFonts', function (done) {
  var stream = merge();
  var cmd = 'convertFonts.sh *.otf --use-font-path-prefix={{FONTS_PATH}}/';

  stream.add(plugins.run(cmd, {
    env: {
      PATH: process.env.PATH + ':~/bin/css3FontConverter'
    },
    cwd: process.cwd() + '/www/fonts'
  }).exec(undefined, done));
});

// See https://github.com/twolfson/gulp.spritesmith
gulp.task('genSprites', function () {
  var sprites = [ 'ui', 'misc', 'subnav' ];

  var spr = function (spriteName) {
    var imgPath = dirs.src + '/img/icons/' + spriteName + '/*.png';
    var spriteData = gulp.src(imgPath)
    .pipe(plugins.spritesmith({
      imgName: 'sprite-' + spriteName + '.png',
      cssName: 'sprite-' + spriteName + '.less',
      cssFormat: 'less',
      algorithm: 'binary-tree',
      cssTemplate: dirs.src + '/img/icons/less.template.handlebars',
      cssOpts: {
        varPrefix: 'spr-' + spriteName + '-',
        spritesheetName: spriteName
      }
    }));

    return spriteData;
  };

  // Sprites stream
  var stream = merge();
  sprites.forEach(function (name) {
    var spriteData = spr(name);

    // Pipe image stream through image optimizer and onto disk
    var imgStream = spriteData.img
      .pipe(gulp.dest(dirs.dist + '/img'));

    // Pipe CSS stream through CSS optimizer and onto disk
    var cssStream = spriteData.css
      .pipe(gulp.dest(dirs.src + '/less/generated'));

    stream.add(spriteData);
    stream.add(imgStream);
    stream.add(cssStream);
  })

  return stream;
});


// ---------------------------------------------------------------------
// | Copy tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('copy', [
  'copy:modernizr',
  'copy:normalize',
  'copy:misc'
]);

gulp.task('copy:modernizr', function () {
  var dir = './bower_components/modernizr';
  return gulp.src(dir + '/modernizr.js')
    .pipe(gulp.dest(dirs.dist + '/js'));
});

gulp.task('copy:normalize', function () {
  var dir = './bower_components/normalize.css';
  return gulp.src(dir + '/normalize.css')
    .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('copy:misc', function () {
  return gulp.src([

    // Copy all files
    dirs.src + '/**/*',

    // Exclude the following files
    // (other tasks will handle the copying of these files)
    '!' + dirs.src + '/less/**/',
    '!' + dirs.src + '/less/',

    '!' + dirs.src + '/jade/**/',
    '!' + dirs.src + '/jade/',

    '!' + dirs.src + '/img/icons/**/',
    '!' + dirs.src + '/img/icons/',
  ], {

    // Include hidden files by default
    dot: true

  }).pipe(gulp.dest(dirs.dist));
});


// ---------------------------------------------------------------------
// | Precompile tasks                                                  |
// ---------------------------------------------------------------------

gulp.task('precompile', [
  'precompile:jade',
  'precompile:less',
  'precompile:js_libs',
  'precompile:js_app',
]);

gulp.task('precompile:jade', function () {
  // Jade variables
  var config = require(dirs.src + '/jade/variables.json');

  return gulp.src(dirs.src + '/jade/pages/*.jade')
    // Compile jade files
    .pipe(plugins.jade({
      locals: config,
      pretty: true
    }))
    .pipe(gulp.dest(dirs.dist));
});

gulp.task('precompile:less', function () {
  return gulp.src(dirs.src + '/less/main.less')
    // Compile less files
    .pipe(plugins.less({
      paths: [ dirs.src + '/less/**/*.less' ]
    }))
    .pipe(gulp.dest(dirs.dist + '/css'));
});

gulp.task('precompile:js_libs', function () {
  return gulp.src([
    // From bower
    './bower_components/requirejs/require.js',
    './bower_components/jquery/dist/jquery.js',
    './bower_components/underscore/underscore.js',
    './bower_components/backbone/backbone.js',
    './bower_components/backbone-filtered-collection/backbone-filtered-collection.js',

    // From src
    dirs.src + '/js/vendor/*.js',
  ])
    .pipe(gulp.dest(dirs.dist + '/js/vendor'));
});

gulp.task('precompile:js_app', function () {
  return gulp.src(dirs.src + '/js/app/**/*.js')
    .pipe(gulp.dest(dirs.dist + '/js/app'));
});


// ---------------------------------------------------------------------
// | Postcompile tasks                                                 |
// ---------------------------------------------------------------------

gulp.task('postcompile', [
  'postcompile:css',
], function () {
  gulp.run('setGlobs');
});

gulp.task('postcompile:css', function () {
  return gulp.src([
    dirs.dist + '/fonts/stylesheet.css',
    dirs.dist + '/css/main.css'
  ])
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions', 'ie >= 8', '> 1%'],
      cascade: false
    }))
    .pipe(plugins.concat('main.css'))
    .pipe(gulp.dest(dirs.dist + '/css'));
});


// ---------------------------------------------------------------------
// | Dev tasks                                                         |
// ---------------------------------------------------------------------

gulp.task('watch', [
  'watch:less',
  'watch:jade',
  'watch:js'
]);

gulp.task('watch:less', function (done) {
  gulp.watch(dirs.src + '/less/**/*.less', function () {
    runSequence(
      ['precompile:less'],
      ['postcompile:css'],
      ['setGlobs'],
    done);
  });
});

gulp.task('watch:jade', function (done) {
  gulp.watch(dirs.src + '/jade/**/*.jade', function () {
    runSequence(
      ['precompile:jade'],
      ['setGlobs'],
    done);
  });
});

gulp.task('watch:js', function (done) {
  gulp.watch(dirs.src + '/js/*.js', function() {
    runSequence(
      ['precompile:js_app'],
    done);
  });
});


// ---------------------------------------------------------------------
// | Main tasks                                                        |
// ---------------------------------------------------------------------

gulp.task('archive', function (done) {
  runSequence(
    'build',
    'archive:create_archive_dir',
    'archive:zip',
  done);
});

gulp.task('build', function (done) {
  runSequence(
    ['clean'],
    ['genSprites'],
    ['copy', 'precompile'],
    ['genFonts'],
    ['postcompile'],
  done);
});

gulp.task('default', ['build']);
